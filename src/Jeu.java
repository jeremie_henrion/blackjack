/* J�r�mie HENRION 3F1 */

import java.util.Scanner;

public class Jeu {

	static Scanner sc  = new Scanner(System.in);

	/**
	 * Transfert des carte
	 * @param a nombre de carte a transferer
	 * @param t talon ou prelever les cartes
	 * @param m main ou placer les cartes
	 */
	public static void transferCards(int nb, Talon t, Main m)
	{		
		for(int i=0;i<nb;i++)
			m.add(t.envoyer());
	}

	/**
	 * Veut-on relancer une partie
	 * @return
	 */
	public static boolean rejouer()
	{
		System.out.println("Voulez-vous rejouer ? (Y/N)");
		String rep = sc.nextLine().toUpperCase();

		switch (rep)
		{
		case"Y": return true;
		case"N": return false;
		default : 	System.out.print("Repondre Y ou N, ");
		return rejouer();
		}
	}

	public static void main(String[] args) throws InterruptedException {

		Talon t;
		JoueurHumain jh;
		JoueurOrdinateur jo;


		System.out.print("Bienvenue � la table, "); 
		Thread.sleep(1000);
		System.out.println("prenez place...\n\n\n");
		Thread.sleep(1000);
		while(true)
		{
			System.out.println("*********************");
			System.out.println("** Nouvelle Partie **");
			System.out.println("*********************\n\n");
			Thread.sleep(1000);

			t  = new Talon();	//initialiqation du talon
			t.melanger();

			jh = new JoueurHumain();			//creation joueur humain
			jo = new JoueurOrdinateur();	//creation joueur ordinateur

			//transfert des cartes en respectant les regles
			transferCards(1, t, jh.m);
			transferCards(1, t, jo.m);
			transferCards(1, t, jh.m);
			transferCards(1, t, jo.m);

			//sans suivre les regles 
			//transferCards(2, t, jh.m);
			//transferCards(2, t, jo.m);


			/* TESTE
			//jo.m.add(new Carte("As","Coeurs"), new Carte("Roi","Coeurs"));	
			//jh.m.add(new Carte("As", "Piques"), new Carte("Roi","Piques"));

			jo.m.add(new Carte("8","Coeurs"), new Carte("7","Coeurs"));	
			jh.m.add(new Carte("7", "Piques"), new Carte("6","Piques"));

			/* affichage des differentes mains */
			System.out.println("Main de l'ordinateur:\n" + jo.m.Liste.get(0).toString());		//affichage de la premiere carte de la main
			System.out.println("?? Carte cach� ??");											//la deuxieme carte est cache
			System.out.println("\nValeur de la main de l'ordinateur: " + jo.valeurDebut() + "\n");//valeur de la main de l'ordinateur

			Thread.sleep(2000);

			System.out.println("Votre main:\n" + jh.toString());							//affichage de la main du joueur
			System.out.println("Valeur de votre main: " + jh.valeur() + "\n");				//affichage de la valeur de la main du joueur


			//verification de Blackjack des le premier tirage
			if (jo.valeur() == 21)
			{
				if(jh.valeur() == 21)
				{
					System.out.println("EXTRAORDINAIRE ! C'est un double BlackJack !! Il y a donc EGALITE !\n");
				}
				else
				{
					System.out.println("Malheureusement pour vous l'ordinateur a fait un BlackJack ! c'est PERDU !\n");					
				}
			}
			else if (jh.valeur() == 21)
			{
				System.out.println("Bravo c'est un blackjack !! La chance est avec vous ajourd'hui ! c'est GAGNE !\n");					
			}
			else {

				do
				{
					if(jh.prendCarte())			//demande de carte
						jh.m.add(t.envoyer());	
					else 
						break;

					System.out.println("Votre main:\n" + jh.toString());							//affichage de la main du joueur
					System.out.println("Valeur de votre main: " + jh.valeur() + "\n");				//affichage de la valeur de la main du joueur

				}
				while(true);

				if(jh.valeur() <= 21 && jo.valeur() < jh.valeur())		//tirage de l'ordinateur tire pas si il gagne/egalite sans tirer
				{
					System.out.print("L'ordinateur tire des cartes");Thread.sleep(1000); System.out.print("."); Thread.sleep(1000); System.out.print("."); Thread.sleep(1000);System.out.println(".\n");

					while(jo.prendCarte())
					{
						jo.m.add(t.envoyer());
					}
				}

				//affichage info ordinateur
				System.out.println("Main de l'ordinateur:\n" + jo.toString());
				System.out.println("Valeur de la main de l'ordinateur: " + jo.valeur());				

				//qui gange
				if(jo.valeur() <= 21)
				{
					if  (jh.valeur() <= 21)
					{
						switch(jh.m.compareTo(jo.m))
						{
						case 1:
							System.out.println("Bravo vous ete trop fort ! Vous avez GAGNE !\n"); break;
						case 0:	
							System.out.println("Malheureusement il y a EGALITE...\n"); break;
						case -1:
							System.out.println("Dommage c'est rate pour cette fois, l'ordinateur a ete meilleur... Vous avez PERDU. \n"); break;

						}
					}
					else
					{
						System.out.println("Dommage c'est rate pour cette fois... Vous avez PERDU. \n");
					}
				}
				else
				{
					System.out.println("Bravo vous ete trop fort ! Vous avez GAGNE !\n");
				}
			}

			//ferme les scanner
			if(!rejouer())
			{
				jh.scClose();
				sc.close();
				break;
			}
		}
	}

}
