/* J�r�mie HENRION 3F1 */

import java.util.ArrayList;

public class SimpleMain {

	ArrayList<Carte> main;

	public SimpleMain()
	{
		main = new ArrayList<Carte>();
	}

	public boolean estValide()
	{
		SimpleTalon t = new SimpleTalon();
		int cpt = 0;

		//verification qu'il n'y a pas de doublon
		for(int i=0; i<main.size(); i++)
		{
			for(int j=0; j<main.size(); j++)
			{
				if( i != j && main.get(i).equals(main.get(j)))
				{
					return false;
				}
			}
		}

		//verification que tt les cartes sont dans un paquet valide
		for(int i=0; i<main.size(); i++)
		{
			for(int j=0; j<t.talon.size(); j++)
			{
				if(main.get(i).equals(t.talon.get(j))) 
					cpt++;
			}
		}

		//renvoi vrai ou faux
		if (cpt == main.size())			
			return true;
		else 
			return false;
	}


	public int valeur()
	{
		int valeur = 0;

		for (int i=0;i<main.size();i++)
		{
			switch (main.get(i).get_Valeur())
			{
			case ("As"): valeur +=11; break;
			case ("2"): valeur+= 2; break;
			case ("3"): valeur+= 3; break;
			case ("4"): valeur+= 4; break;
			case ("5"): valeur+= 5; break;
			case ("6"): valeur+= 6; break;
			case ("7"): valeur+= 7; break;
			case ("8"): valeur+= 8; break;
			case ("9"): valeur+= 9; break;
			case ("10"): valeur+= 10; break;
			case ("Valet"): valeur+= 10; break;
			case ("Dame"): valeur+= 10; break;
			case ("Roi"): valeur+= 10; break;
			}
		}

		//modifie la valeur de l'As en cas de besoin
		for (int i=0;i<main.size();i++)
		{
			if (main.get(i).get_Valeur() == "As")
			{
				if (valeur > 21)
					valeur -=10;					
			}
		}
		return valeur;
	}

	public boolean compareTo(SimpleMain m)
	{
		if (m.valeur() > 21)
			return true;
		if(this.valeur() > 21)
			return false;
		if(m.valeur() > this.valeur())
			return false;
		else
			return true;

	}

	public String toString()
	{
		String paquet = "";
		for(int i=0;i<main.size();i++)
		{
			paquet += main.get(i).toString() + "\n";
		}
		return paquet;
	}

	public void add(Carte a)
	{
		main.add(a);
	}


	public static void main(String[] args) {

		SimpleTalon t = new SimpleTalon();
		SimpleMain m = new SimpleMain();
		SimpleMain n = new SimpleMain();


		m.add(t.envoyer());
		m.add(t.envoyer());
		m.add(t.envoyer());
		m.add(t.envoyer());

		n.add(t.envoyer());
		n.add(t.envoyer());


		System.out.println(m.toString());
		System.out.println();
		System.out.println();
		System.out.println(n.toString());

		System.out.println();
		System.out.println();

		System.out.println(m.estValide() + " valeur: "+ m.valeur());

		System.out.println(n.estValide() + " valeur: " + n.valeur());
		System.out.println();


		System.out.println("Main 2 bat Main 1 : " + n.compareTo(m));
		System.out.println("Main 1 bat Main 2 : " + m.compareTo(n));

	}

}
