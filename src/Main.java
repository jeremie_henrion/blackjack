/* J�r�mie HENRION 3F1 */

public class Main extends CardSet implements Comparable{

	/* CONSTRUCTEUR SANS CARTE */
	public Main()
	{
		super();
	}

	/* CONSTRUCTEUR DE PLUSIEURS CARTES */
	public Main(Carte... b)
	{
		super();
		for(int i = 0;i<b.length;i++)
		{
			Liste.add(b[i]);
		}
	}

	/**
	 * Renvoi vrai si la main ne contient que des cartes d'un jeu standard de 52 cartes, Faux sinon
	 */
	public boolean estValide()
	{
		if(super.estValide())
		{
			int cpt = 0;
			Talon t = new Talon();
			//verification que tt les cartes sont dans un paquet valide
			for(int i=0; i<Liste.size(); i++)
			{
				for(int j=0; j<t.Liste.size(); j++)
				{
					if(Liste.get(i).equals(t.Liste.get(j))) 
						cpt++;
				}
			}
			//renvoi vrai ou faux
			if (cpt == Liste.size())			
				return true;
			else 
				return false;
		}
		else 
			return false;
	}

	/**
	 * Valeur du paquet
	 * @return somme des valeurs des cartes
	 */
	public int valeur()
	{
		int valeur = 0, nbAs=0;	//valeur de la main et nombre d'As dans la main

		for (int i=0; i<Liste.size(); i++)
		{
			switch(Liste.get(i).get_Valeur())
			{
			case"As": valeur += 11; nbAs++; break;
			case"Valet": valeur+=10; break;
			case"Dame": valeur+=10; break;
			case"Roi": valeur+=10; break;
			default: valeur += Integer.parseInt(Liste.get(i).get_Valeur()); break;
			}	
		}

		//modifie la valeur de l'As en cas de besoin
		for (int i=0;i<Liste.size();i++)
		{
			if (Liste.get(i).get_Valeur() == "As" && nbAs > 0)
			{
				if (valeur > 21)
					valeur -=10;
				nbAs--;
			}
		}
		return valeur;
	}

	/**
	 * Compare qui gagne entre la main actuelle et la main donn�e en parametres
	 */
	@Override
	public int compareTo(Object o)
	{
		Main m = (Main) o;
		if(m.valeur() == this.valeur())
			return 0;
		if(m.valeur() > this.valeur())
			return -1;
		else 
			return 1;
	}

	//ajoute une carte a la main
	public void add(Carte a)
	{
		Liste.add(a);
	}

	public void add(Carte... a)
	{
		for(int i=0; i<a.length;i++)
		{
			Liste.add(a[i]);
		}
	}

	public static void main(String[] args) {

		Talon t = new Talon();
		Main m = new Main();
		Main n = new Main();


		m.add(t.envoyer());
		m.add(t.envoyer());
		m.add(t.envoyer());
		m.add(t.envoyer());

		n.add(t.envoyer());
		n.add(t.envoyer());


		System.out.println(m.toString());
		System.out.println();
		System.out.println();
		System.out.println(n.toString());

		System.out.println();
		System.out.println();

		System.out.println(m.estValide() + " valeur: "+ m.valeur());

		System.out.println(n.estValide() + " valeur: " + n.valeur());
		System.out.println();


		System.out.println("Main 2 bat Main 1 : " + n.compareTo(m));
		System.out.println("Main 1 bat Main 2 : " + m.compareTo(n) + "\n");


		Carte a = new Carte("Roi", "Coeurs");
		Carte b = new Carte("Dame", "Coeurs");

		Main o = new Main(a,b);

		System.out.println(o.toString());		
	}

}
