/* J�r�mie HENRION 3F1 */

import java.util.ArrayList;

public abstract class CardSet {

	ArrayList<Carte> Liste;

	String valeur[] = {"As","2","3","4","5","6","7","8","9","10","Valet","Dame","Roi"};
	String couleur[] = {"Carreaux", "Coeurs", "Trefles", "Piques"};

	/* CONSTRUCTEUR */
	public CardSet()
	{
		this.Liste = new ArrayList<Carte>();
	}

	/**
	 * Verifie que le paquet est valide
	 * @return
	 */
	public boolean estValide()
	{
		for(int i=0; i<Liste.size(); i++)
		{
			for(int j=0; j<Liste.size(); j++)
			{
				if( i != j && Liste.get(i).equals(Liste.get(j)))
				{
					return false;
				}
			}
		}

		return true;	
	}

	/**
	 * Affiche les cartes du paquet
	 */
	public String toString()
	{
		String paquet = "";
		for(int i=0;i<Liste.size();i++)
		{
			paquet += Liste.get(i).toString() + "\n";
		}
		return paquet;
	}

	//guetteur
	public ArrayList<Carte> getList()
	{
		return Liste;
	}


}
