/* J�r�mie HENRION 3F1 */

public class JoueurOrdinateur extends Joueur {

	public JoueurOrdinateur()
	{
		super();
	}

	/**
	 * L'odinateur prend des Cartes
	 */
	public boolean prendCarte()
	{
		if(m.valeur() <= 16)
			return true;
		else 
			return false;
	}

	/**
	 * Affiche la valeur de la main de l'odinateur sans prendre en compte la carte cache
	 * @return
	 */
	public int valeurDebut()
	{
		String a = m.getList().get(0).get_Valeur();
		if(a.equals("Dame") || a.equals("Valet") || a.equals("Roi"))
			return 10;
		else if (a.equals("As"))
			return 11;
		else
			return Integer.parseInt(m.getList().get(0).get_Valeur());
	}


}
