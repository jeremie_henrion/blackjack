/* J�r�mie HENRION 3F1 */

public abstract class Joueur {

	Main m;	//main du joueur


	public Joueur()
	{
		this.m = new Main();
	}

	/**
	 * Valeur de la main du joueur
	 * @return somme des valeurs des cartes de la main du joueur
	 */
	public int valeur()
	{
		return m.valeur();
	}

	/**
	 * Affiche les Cartes du joueur
	 */
	public String toString()
	{
		return m.toString();
	}


	public abstract boolean prendCarte();

	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}

}
