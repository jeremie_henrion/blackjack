/* J�r�mie HENRION 3F1 */

import java.util.ArrayList;

public class Talon extends CardSet{

	/* Constructeur */
	public Talon()
	{
		super();

		for(int i=0; i<4; i++)
		{
			for(int j=0; j<13; j++)
			{
				Liste.add(new Carte (valeur[j], couleur[i]));
			}
		}
	}

	/**
	 * Verifie qu'un paquet est complet
	 * @return
	 */
	public boolean estComplet()
	{
		if(!estValide() || Liste.size()!=52) return false;

		//creation paquet test forcement complet;
		ArrayList<Carte> paquetTest = new ArrayList<Carte>();
		for(int i=0; i<4; i++)
		{
			for(int j=0; j<13; j++)
			{
				paquetTest.add(new Carte (valeur[j], couleur[i]));
			}
		}

		//supprimer Carte du paquet test si est dans le talon
		for(int i=0; i<Liste.size(); i++)
		{
			for(int j=0; j<paquetTest.size(); j++)
			{
				if(Liste.get(i).equals(paquetTest.get(j)))
				{
					paquetTest.remove(j);
				}
			}
		}

		if(paquetTest.size() > 0)
			return false;
		else 
			return true;
	}

	/**
	 * Melange le talon selon le mod�le Gilbert-Shannon-Reeds 
	 */
	public void melanger()
	{
		//2 partie du paquet
		ArrayList<Carte> paquet1 = new ArrayList<Carte>();
		ArrayList<Carte> paquet2 = new ArrayList<Carte>();

		int taille = Liste.size();

		for(int i=0; i<7; i++)
		{			
			for(int j = 0; j < taille; j++)
			{
				double val = Math.random();
				if(val > 0.5)
					paquet1.add(Liste.get(j));
				else 
					paquet2.add(Liste.get(j));
			}

			Liste.clear();	//vider paquet original

			//remplire paquet principale
			for(int j=0; j < taille ;j++)
			{
				double alea = paquet1.size() / (paquet1.size() + paquet2.size());
				double a = Math.random();
				if(a < alea)
				{
					Liste.add(paquet1.get(0));
					paquet1.remove(0);

				}
				else
				{
					Liste.add(paquet2.get(0));		
					paquet2.remove(0);

				}
			}
		}
		paquet1.clear();	//vider les 2 partie du paquet
		paquet2.clear();
	}

	/**
	 * Envoi une Carte et la supprime du talon
	 * @return Une Carte
	 */
	public Carte envoyer()
	{
		Carte c = Liste.get(0);
		Liste.remove(0);
		return c;

		//VERSION AVANT MELANGER
		/*
		int val = (int) (Math.random() * Liste.size());
		Carte c = Liste.get(val);
		Liste.remove(val);
		return c;
		 */
	}


	public static void main(String[] args) {

		Talon t = new Talon();
		System.out.println(t.toString());

		System.out.println("vrai valide : " + t.estValide());
		t.Liste.get(2).set_Couleur("Coeurs");
		System.out.println("false valide : " + t.estValide());




	}

}
