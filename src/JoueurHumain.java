/* J�r�mie HENRION 3F1 */

import java.util.Scanner;

public class JoueurHumain extends Joueur {

	Scanner sc;

	public JoueurHumain()
	{
		super();
		this.sc = new Scanner(System.in);
	}

	/**
	 * Demande si le joueur veut une Carte
	 */
	public boolean prendCarte()
	{
		if(m.valeur() > 21)
			return false;
		System.out.println("Voulez-vous prendre une autre carte ?");
		System.out.println("YES / NO");

		String rep = sc.nextLine().toUpperCase();

		switch (rep)
		{
		case"YES": return true;
		case"NO": return false;
		default : 	System.out.print("Repondre YES ou NO, ");
		return prendCarte();
		}
	}

	/**
	 * Ferme le scanner
	 */
	public void scClose()
	{
		sc.close();
	}



	public static void main(String[] args) {

		JoueurHumain j = new JoueurHumain();
		System.out.println(j.prendCarte());

	}

}
