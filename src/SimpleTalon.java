/* J�r�mie HENRION 3F1 */

import java.util.ArrayList;

public class SimpleTalon {

	String valeur[] = {"As","2","3","4","5","6","7","8","9","10","Valet","Dame","Roi"};
	String couleur[] = {"Carreaux", "Coeurs", "Trefles", "Piques"};
	ArrayList<Carte> talon;

	public SimpleTalon()
	{
		talon = new ArrayList<Carte>();

		for(int i=0; i<4; i++)
		{
			for(int j=0; j<13; j++)
			{
				talon.add(new Carte (valeur[j], couleur[i]));
			}
		}
	}


	public boolean estValide()
	{
		for(int i=0; i<talon.size(); i++)
		{
			for(int j=0; j<talon.size(); j++)
			{
				if( i != j && talon.get(i).equals(talon.get(j)))
				{
					return false;
				}
			}
		}

		return true;	
	}

	public boolean estComplet()
	{
		if(!estValide() || talon.size()!=52) return false;

		ArrayList<Carte> paquetTest = new ArrayList<Carte>();

		for(int i=0; i<4; i++)
		{
			for(int j=0; j<13; j++)
			{
				paquetTest.add(new Carte (valeur[j], couleur[i]));
			}
		}

		for(int i=0; i<talon.size(); i++)
		{
			for(int j=0; j<paquetTest.size(); j++)
			{
				if(talon.get(i).equals(paquetTest.get(j)))
				{
					paquetTest.remove(j);
				}
			}
		}

		if(paquetTest.size() > 0)
			return false;
		else 
			return true;
	}

	public String toString()
	{
		String paquet = "";
		for(int i=0;i<talon.size();i++)
		{
			paquet += talon.get(i).toString() + "\n";
		}
		return paquet;

	}

	public Carte envoyer()
	{
		int val = (int) (Math.random() * talon.size());
		Carte c = talon.get(val);
		talon.remove(val);
		return c;
	}

	public static void main(String[] args) {

		SimpleTalon t = new SimpleTalon();
		/*
		for (int i=0;i<t.talon.size();i++)
		{
			System.out.println(t.talon.get(i).toString());
		}
		 */
		System.out.println("vrai valide : " + t.estValide());
		System.out.println("vrai complet : " + t.estComplet());

		t.talon.get(2).set_Couleur("Coeurs");

		System.out.println("faux valide : " + t.estValide());
		System.out.println("faux complet : " + t.estComplet());

		System.out.println();
		System.out.println();
		System.out.println(t.toString());



	}

}
