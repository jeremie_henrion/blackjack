/* J�r�mie HENRION 3F1 */

public class Carte {

	private String valeur;
	private String couleur;


	/* CONSTRUCTEUR */
	/**
	 * Cr�e une Carte
	 * @param valeur String de la valeur de la Carte
	 * @param couleur String de la couleur de la Carte
	 */
	public Carte (String valeur, String couleur)
	{
		this.valeur = valeur;
		this.couleur = couleur;
	}

	/* GETTER */
	/**
	 * Retourne la valeur de la Carte
	 * @return
	 */
	public String get_Valeur()
	{
		return valeur;
	}

	/**
	 * Retourne la couleur de la Carte
	 * @return
	 */
	public String get_Couleur()
	{
		return couleur;
	}

	@Override
	/**
	 * Renvoi Vrai si la Carte actuelle est egale a la Carte pass� en parametre
	 */
	public boolean equals(Object o) {
		if (o instanceof Carte)
		{
			Carte a = (Carte) o;
			if (a.get_Couleur() == this.get_Couleur() && a.get_Valeur() == this.get_Valeur())
				return true;
			else
				return false;
		}
		return false;
	}

	/**
	 * Affiche la Carte |Valeur| de |Couleur|
	 */
	public String toString()
	{
		return valeur + " de " + couleur;
	}

	/**
	 * Modifie la couleur de la carte
	 * @param a String de la couleur
	 */
	public void set_Couleur(String a)
	{
		couleur = a;
	}

	public static void main(String[] args) {

		Carte a = new Carte("Roi", "Trefle");
		Carte b = new Carte("Dame", "Carreau");
		Carte c = new Carte("Roi", "Trefle");

		System.out.println("Roi = ? --> " + a.get_Valeur());
		System.out.println("Trefle = ? --> " + a.get_Couleur());
		System.out.println("Carreau = ? --> " + b.get_Couleur());
		System.out.println("Dame = ? --> " + b.get_Valeur());

		System.out.println();
		System.out.println("False? --> " + a.equals(b));
		System.out.println("True? --> " + a.equals(c));

		System.out.println();

		System.out.println(a.toString());
		System.out.println(b.toString());
		System.out.println(c.toString());

	}




}
